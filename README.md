# GNUKhata Issue Tracker

This repository is used to track issues coming from the gkapp "Report Bug" section.

### Useful Links:

- [This repository's Issues page](../../issues)
- Website: https://gnukhata.org
- gkcore: https://gitlab.com/gnukhata/gkcore
- gkapp: https://gitlab.com/gnukhata/gkapp
